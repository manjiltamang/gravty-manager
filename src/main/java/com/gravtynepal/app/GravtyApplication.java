package com.gravtynepal.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GravtyApplication {

	public static void main(String[] args) {
		SpringApplication.run(GravtyApplication.class, args);
	}

}
