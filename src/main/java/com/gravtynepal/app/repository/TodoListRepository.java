package com.gravtynepal.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.gravtynepal.app.models.TodoList;

public interface TodoListRepository extends CrudRepository<TodoList, Integer>{

}
