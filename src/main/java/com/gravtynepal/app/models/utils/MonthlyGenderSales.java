package com.gravtynepal.app.models.utils;

public interface MonthlyGenderSales {
	String getGender();	
    String getMonth();
	String getCount();

}

