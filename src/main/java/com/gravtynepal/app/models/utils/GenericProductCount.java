package com.gravtynepal.app.models.utils;

public interface GenericProductCount {
	String getType();
    int getCount();
}
