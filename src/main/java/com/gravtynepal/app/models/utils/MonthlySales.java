package com.gravtynepal.app.models.utils;

public interface MonthlySales {
	String getMonth();
    int getSales();
}


