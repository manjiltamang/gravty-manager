package com.gravtynepal.app.models.utils;

public interface GenericGenderCount {
	String getGender();
    int getCount();
}
